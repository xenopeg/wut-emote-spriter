var data = require('./data.json');
var fs = require('fs');
var Jimp = require("jimp");

fs.readdir('./imgs', async function (err, p) {
  if (err){
    throw err;
  }

  var files = [];
  p.forEach( f => {
    if( fs.lstatSync('./imgs/'+f).isDirectory() ){
      let ffs = fs.readdirSync('./imgs/'+f);
      ffs.forEach( ff => {
        files.push( [f, ff] );
      } );
    }else{
      files.push( [null, f] );
    }
  })
  
  var imgs = [];
  await Promise.all(files.map( f => (async function(){
    var path = "./imgs/" + f.filter( x => x!==null ).join('/');
    var name = f[1].replace('.png', '');
    var i;
    try{
      i = await Jimp.read(path);
      imgs.push({
        name: name,
        width: i.bitmap.width,
        height: i.bitmap.height,
        sheet: 'sheet'+( f[0] ? ('_'+f[0]) :'' ),
        img: i
      });
    }catch(e){
      console.log(path)
      console.log(e)
    }
  })()));

  var t_height = imgs.reduce( (p, c) => {
    return p + ( data.animated.indexOf(c.name) === -1 ? c.height : 0 )
  }, 0 );
  var max_width = imgs.reduce( (p, c) => {
    return p > ( data.animated.indexOf(c.name) === -1 ? c.width : 0 ) ? p : c.width
  }, 0 );

  var sheets = {};
  imgs.forEach( i => {
    if( !sheets[i.sheet] ){
      sheets[i.sheet] = {
        max_width: 0,
        t_height: 0,
        c_height: 0
      }
    }

    sheets[i.sheet].t_height += i.height;
    sheets[i.sheet].max_width = sheets[i.sheet].max_width > i.width ? sheets[i.sheet].max_width : i.width;
  });

  for( let i = 0, K = Object.keys(sheets) ; i < K.length ; i++ ){
    sheets[K[i]].img = await new Promise( ( res, rej ) => {
      new Jimp( sheets[K[i]].max_width, sheets[K[i]].t_height, function (err, image) {
        res(image);
      })
    })
  }

  var css = '';
  imgs.forEach( i => {
    console.log(i.name+' 1');
    var c = '';
    var css_obj = {
      "display": "block",
      "clear": "none",
      "float": "left",
      "background-image": `url("%%${data.animated.indexOf(i.name) === -1?i.sheet.replace('_','-'):i.name}%%")`,
      "background-position": `0px -${data.animated.indexOf(i.name) === -1?sheets[i.sheet].c_height:0}px`,
      "width": `${i.width}px`,
      "height": `${i.height}px`
    };
    
    if( data.animated.indexOf(i.name) === -1 ){
      sheets[i.sheet].img.composite( i.img, 0, sheets[i.sheet].c_height );
      sheets[i.sheet].c_height += i.height;
    }

    console.log(i.name+' 2');
    var extra = data["extra-props"].find( x => x.name === i.name);
    if( extra ){
      Object.keys(extra.props).forEach( k => css_obj[k] = extra.props[k] );
    }

    var hover_alt = data["hover"].find( x => x.alt === i.name);
    var alias = [hover_alt?hover_alt.name:i.name];
    if( data["alts"][alias[0]] ){
      alias = alias.concat( 
        Array.isArray(data["alts"][alias[0]])
        ?data["alts"][alias[0]]
        :[data["alts"][alias[0]]] 
      )
    }
    console.log(alias)
    c = alias.map( a => `a[href|="/${a}"]`+(hover_alt?':hover':'') ).join(',\n') + '{\n'
      + Object.keys(css_obj).map( k => `  ${k}: ${css_obj[k]};`).join('\n')
      + '\n}';

    var blank = data["blank"].find( x => x.name === i.name);
    if( blank ){
      let em = blank.props;
      
      let em_c = alias.map( a => `a[href|="/${a}"] em` ).join(',\n') + '{\n'
        + Object.keys(em).map( k => `  ${k}: ${em[k]};`).join('\n')
        + '\n}';

      c = em_c + '\n' + c;
    }
    css+=c+'\n';
  });
  for( let i = 0, K = Object.keys(sheets) ; i < K.length ; i++ ){
    sheets[K[i]].img.write(K[i]+'.png');
  }
  fs.writeFileSync('sheet.css', css);
});